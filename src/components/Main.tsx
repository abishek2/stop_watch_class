import React from "react";
import "./Main.css";
import { AiOutlineClockCircle } from 'react-icons/ai';

interface Props {

}
interface State {
  timerVal: number[],
}

class Main extends React.Component<Props, State>{
  timerCount: number;
  timerId:NodeJS.Timeout;
  isStarted:boolean;//to disable start button when timer is already started
  constructor() {
    super({});
    this.state = { timerVal: [0, 0, 0] };
    this.timerCount = 0;
    this.isStarted=false;
    //just initializing timerId nothing more
    this.timerId=setTimeout(()=>{},0);
    clearTimeout(this.timerId);
  }

  startTimer() {
    this.timerId=setInterval(() => { this.timerCount++; this.setTime(); },1000);
    this.isStarted=true;
  }
  resetTimer(){
    this.timerCount=0;
    clearTimeout(this.timerId);
    this.setState({timerVal:[0,0,0]})
    this.isStarted=false;
  }
  setTime() {
    let tempTimerVal = this.timerCount;
    let newTime: number[] = [0, 0, 0];
    if (tempTimerVal > 86400) {
      tempTimerVal = 0;
    }
    if (tempTimerVal / 3600 >= 1) {
      newTime[0] = Math.floor(tempTimerVal / 3600);
      tempTimerVal %= 3600;
    }
    if (tempTimerVal / 60 >= 1) {
      newTime[1] = Math.floor(tempTimerVal / 60);
      tempTimerVal %= 60;
    }
    newTime[2] = tempTimerVal;
    this.setState({ timerVal: newTime })
  }
  getTwoDigit(num:number){
    return `0${num}`;
  }
  getTime(tempArr:number[]){
    let time:string;
    /*this part take array element and format them like two digit number separated by colon*/
    console.log(tempArr)
    time=`${tempArr[0]>=10?tempArr[0]:this.getTwoDigit(tempArr[0])} : ${tempArr[1]>=10?tempArr[1]:this.getTwoDigit(tempArr[1])} : ${tempArr[2]>=10?tempArr[2]:this.getTwoDigit(tempArr[2])}`;
    return time;
  }
  render() {
    return (<main className="mainBox">
      <div className="backgroundBox">
        <div className="contentBox">
          <section className="titleSection">
            <div className="title">React Stopwatch </div>
            <AiOutlineClockCircle />
          </section>
          <div className="stopWatchBox">
            <div className="time">{this.getTime(this.state.timerVal)}</div>
            <div className="buttons">
              <button onClick={this.isStarted?()=>{alert("Hey, timer is already started")}:this.startTimer.bind(this)} className="button">Start</button>
              <button onClick={this.resetTimer.bind(this)} className="button">Reset</button>
            </div>
          </div>
        </div>
      </div>
    </main>);
  }
}

export default Main;